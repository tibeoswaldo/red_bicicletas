# README #

# CURSO NODEJS. PROYECTO BICICLETAS
Desarrollo del lado servidor: NodeJS, Express y Mongo 

### Instalar el Proyecto
_Debe bajar el proyecto en una carpeta en su computador, luego instalar como:_

$ npm install

### Postman
_Para ejecutar con Postman primero debe loguearse:_
_El metodo utilizado es POST:_

_http://localhost:3000/api/auth/authenticate
En el pestaña BODY en la opcion "raw", despues de elegir JSON, debe introducir el siguiente codigo, por ejemplo:_
```
{
    "email":"test10@gmail.com",
    "password":"uc9c3fhMUdxyzFYMJ"
}
```
### Resultado
_Luego de ejecutar, la autenticacion, tendra el siguiente resultado, por ejemplo:_
```
{
    "message": "Usuario encontrado",
    "data": {
        "usuario": {
            "verificado": true,
            "_id": "5f6028fa469ba84a200ce5eb",
            "nombre": "test",
            "email": "test10@gmail.com",
            "password": "$2b$10$pqHrXj9Dz4lhwGmCGcgaTe8fr49X1XS2ztMpPhpqKnqRahxzI1XR6",
            "__v": 0
        },
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmNjAyOGZhNDY5YmE4NGEyMDBjZTVlYiIsImlhdCI6MTYwMDI2NTY0NywiZXhwIjoxNjAwODcwNDQ3fQ.K7PwP8xwYO-R0he4eRQJU-kukDgPpPxHPSml_ZHT7h4"
    }
}
```
_Ahora para ejecutar los demas web services debe copiar el token generado_

### Web Services disponibles:
_Para ejecutar un servicio web, debe poner en su cabecera, el token copiado._
_Esta es la lista de Web services diponibles: _
```
POST: http://localhost:3000/api/bicicletas/create
GET: http://localhost:3000/api/bicicletas
DELETE: http://localhost:3000/api/bicicletas/delete
PUT: http://localhost:3000/api/bicicletas/update
```
