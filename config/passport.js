const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');

passport.use(new FacebookTokenStrategy({
        clientID: process.env.FACEBOOK_APP_ID,
        clientSecret: process.env.FACEBOOK_APP_SECRET,
        fbGraphVersion: 'v3.0'
    }, function (accessToken, refreshToken, profile, done) {
        try {
            console.log("FaceToken--profile", profile);
            Usuario.findOneOrCreateByFacebook(profile, function (err, user) {
                if (err) console.log('err ' + err)
                return done(err, user);
            });
        } catch(err2) {
            console.log(err2);
            return done(err2, null);
        }
    }
));

passport.use(new LocalStrategy(
    function(email, password, done){
        Usuario.findOne({email: email}, function(err, usuario) {
            if (err) return done(err);
            if (!usuario) return done(null, false, { message: 'email no existe o incorrecto' });
            if (!usuario.validPassword(password)) return done(null,false,{ message: 'password incorrecto' });

            return done(null, usuario);
        });
    }
));

// callbackURL: process.env.HOST + '/auth/google/callback'
passport.use(
    new GoogleStrategy({
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: 'https://red-bicicletas-tibe.herokuapp.com/auth/google/callback'
    }, (accessToken, refreshToken, profile, cb) => {
      console.log(profile);
      Usuario.findOneOrCreateByGoogle(profile, (err, user) => {
        return cb(err, user);
      });
    })
);

passport.serializeUser(function(user, cb ){
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb){
    Usuario.findById(id, function(err, usuario ){
        cb(null, usuario);
    })
});

module.exports = passport
