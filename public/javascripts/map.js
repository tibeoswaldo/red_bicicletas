var map = L.map('main_map', {
    center: [-19.047818, -65.259662], //ciudad de Sucre, Bolivia
    zoom: 13
});

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(map);

/*
L.marker([-19.035444, -65.273607]).addTo(map);
L.marker([-19.047043, -65.258326]).addTo(map);
L.marker([-19.043250, -65.244477]).addTo(map);
*/

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})
