var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var URL = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {
    describe('GET BICICLETAS / ', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'urbana', [-19.035444, -65.273607]);
            Bicicleta.add(a);

            request.get(URL, function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('STATUS 200', (done) => {
          var headers = {'Content-Type': 'application/json'};
          var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "lat": -54.36, "lng": -74.35 }';
          request.post({
              headers: headers,
              url: `${URL}/create`,
              body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });
        });
    });
    
    describe('PUT BICICLETAS /update', () => {
        it('STATUS 200', (done) => {
            var headers = {'Content-Type': 'application/json'};

            var a = new Bicicleta(20, 'rojo', 'urbana', [-19.035444, -65.273607]);
            Bicicleta.add(a);

            var aBici = { "id": 20, "color": "rojito", "modelo": "urbana", "lat": -54.36, "lng": -74.35 }; //Datos a actualizar

            const options = {
                url: URL + '/update',
                headers: headers,
                json: true,
                body: aBici
            };
            
            request.put(options, (err, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(20).color).toBe("rojito");
                done();
            }); 

        });
    });

    describe('DELETE BICICLETAS /delete', () => {
        it('STATUS 204', (done) => {
            var headers = {'Content-Type': 'application/json'};

            var a = new Bicicleta(30, 'rojo', 'urbana', [-19.035444, -65.273607]);
            Bicicleta.add(a);

            var aBici = { "id": 30  }; //Dato a borrar

            const options = {
                url: URL + '/delete',
                headers: headers,
                json: true,
                body: aBici
            };
            
            request.delete(options, (err, response, body) => {
                expect(response.statusCode).toBe(204);
                done();
            }); 

        });
    });
    
});