var mongoose = require('mongoose');
var Usuario = require('../../models/usuario');
var Bicicleta = require('../../models/bicicleta');
var Reserva = require('../../models/reserva');

describe('Testing Api Reservas ', function() {

    beforeAll((done) => { mongoose.connection.close(done) });
    
    beforeEach(function(done){
        mongoose.disconnect();
        var mongoDB = "mongodb://localhost/testapidb";
        mongoose.connect(mongoDB, { useNewUrlParser:true, useCreateIndex: true, });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function( err, success ){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success)  {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Reserva.allReservas', () => {
        it('comienza vacia', (done)  => {
            Reserva.allReserva(function(err, reservas){
                expect(reservas.length).toBe(0);
                done();
            });
        });
    });
    
});